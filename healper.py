def days_to_units_sec(num_of_days, conversion_unit):
    if conversion_unit == "hours":
        return f"{num_of_days} days are {num_of_days * 24} hours"
    elif conversion_unit == "minutes":
        return f"{num_of_days} days are {num_of_days * 24 * 60}  minutes"
    else:
        return "unsupported unit"


def validate_execute(days_and_units_dic):
    try:
        user_input_number = int(days_and_units_dic["days"])
        # condition to check and validate only for integer values
        if user_input_number > 0:
            calculation_to_seconds = days_to_units_sec(user_input_number, days_and_units_dic["unit"])
            print(calculation_to_seconds)
        elif user_input_number == 0:
            print("its zero")
        else:
            print("entered is negitive")
    except ValueError:
        print("Not a valid input.")

user_input_msg = "Please enter the input?\n"